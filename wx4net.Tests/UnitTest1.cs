using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var d1 = new Dictionary<int, string>() { [1] = "one" };
            var d2 = new Dictionary<int, string>() { [1] = "un", [2] = "deux" };

            var merged1 = d1.Concat(d2).ToLookup(x => x.Key, x => x.Value)
                .ToDictionary(x => x.Key, g => g.First());
            Assert.Pass();
        }
    }
}
﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    EHttpContent
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 2:00 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 2:00 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace wx4net.Extentions
{
    public static class EHttpContent
    {
        public static async Task<T> ReadAsAsync<T>(this HttpContent httpContent)
        {
            var contentStr = await httpContent.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(contentStr);
        }

        public static T ReadAs<T>(this HttpContent httpContent)
        {
            var contentStr = httpContent.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(contentStr);
        }
    }
}

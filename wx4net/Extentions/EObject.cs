﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    EObject
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 10:39 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 10:39 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace wx4net.Extentions
{
    public static class EObject
    {
        public static HttpContent ToHttpContent(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}

﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    EHttpClient
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 2:21 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 2:21 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace wx4net.Extentions
{
    public static class EHttpClient
    {
        public static T GetAs<T>(this HttpClient httpClient, string requestUri)
        {
            var result = httpClient.GetStringAsync(requestUri).Result;
            return JsonConvert.DeserializeObject<T>(result);
        }

        public static async Task<T> GetAsAsync<T>(this HttpClient httpClient, string requestUri)
        {
            var result = await httpClient.GetStringAsync(requestUri);
            return JsonConvert.DeserializeObject<T>(result);
        }

        public static T PostAs<T>(this HttpClient httpClient, string requestUri, HttpContent content)
        {
            using (var response = httpClient.PostAsync(requestUri, content).Result)
            {
                return response.Content.ReadAs<T>();
            }
        }

        public static async Task<T> PostAsAsync<T>(this HttpClient httpClient, string requestUri, HttpContent content)
        {
            using (var response = await httpClient.PostAsync(requestUri, content))
            {
                return response.Content.ReadAs<T>();
            }
        }

        public static T PostAs<T>(this HttpClient httpClient, string requestUri, IEnumerable<KeyValuePair<string, string>> nameValueCollection)
        {
            return httpClient.PostAs<T>(requestUri, new FormUrlEncodedContent(nameValueCollection));
        }

        public static async Task<T> PostAsAsync<T>(this HttpClient httpClient, string requestUri, IEnumerable<KeyValuePair<string, string>> nameValueCollection)
        {
            return await httpClient.PostAsAsync<T>(requestUri, new FormUrlEncodedContent(nameValueCollection));
        }
    }
}

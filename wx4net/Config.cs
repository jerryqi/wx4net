﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    Config
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 11:42 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 11:42 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
namespace wx4net
{
    public class Config
    {
        public Config(string appid, string secret)
        {
            AppId = appid;
            Secret = secret;
            AppInfo = AppInfo.Replace("APPID", appid).Replace("SECRET", secret);
        }

        public readonly string AppInfo = "appid=APPID&secret=SECRET";

        public string AppId { get; private set; }

        public string Secret { get; private set; }

        public string ApiBase => "https://api.weixin.qq.com";

        public string GetPattern(string url)
        {
            return url + (url.IndexOf('?') > 0 ? "?" : "&");
        }

        public string GetUrlWithAppInfo(string url, string arg = null)
        {
            var mergeUrl = string.Format("{0}/{1}{2}", ApiBase, GetPattern(url), AppInfo);
            return string.IsNullOrEmpty(arg) ? mergeUrl : string.Format("{0}&{1}", mergeUrl, arg);
        }
    }
}

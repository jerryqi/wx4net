﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    User
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/25/2019 5:28 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/25/2019 5:28 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Net.Http;
using wx4net.Extentions;
using wx4net.MiniApp.DTO.Auth;

namespace wx4net.MiniApp.Services
{
    public class User : ServiceBase
    {
        public User(MiniAppConfig config, MiniAppToken token, HttpClient client)
            : base(config, token, client) { }

        private string code2SessionUrl = "sns/jscode2session?grant_type=authorization_code";
        public Code2SessionResult Code2Session(string jscode)
        {
            return GetWithAppInfo<Code2SessionResult>(code2SessionUrl, string.Format("js_code={0}", jscode));
        }

        private string getPaidUnionIdUrl = "wxa/getpaidunionid";
        public GetPaidUnionIdResult GetPaidUnionId(GetPaidUnionIdRequest request)
        {
            return GetWithAppInfo<GetPaidUnionIdResult>(getPaidUnionIdUrl, request.UrlPattern);
        }
    }
}

﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    CustomerServiceMessage
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 11:53 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 11:53 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using wx4net.Extentions;
using wx4net.MiniApp.DTO.CustomerServiceMessage;

namespace wx4net.MiniApp.Services
{
    public class CustomerServiceMessage : ServiceBase
    {
        public CustomerServiceMessage(MiniAppConfig config, MiniAppToken token, HttpClient client)
            : base(config, token, client) { }

        private readonly string setTypingUrl = "cgi-bin/message/custom/typing";
        public SetTypingResult SetTyping(SetTypingRequest request)
        {
            return PostWithToken<SetTypingResult>(setTypingUrl, request);
        }

        private readonly string uploadTempMediaUrl = "cgi-bin/media/upload?type=image";
        public UploadTempMediaResult UploadTempMedia(FileStream fileStream)
        {
            using var content = new MultipartFormDataContent();
            using var fileContent = new StreamContent(fileStream, (int)fileStream.Length);
            content.Add(fileContent, "media", fileStream.Name);
            return _client.PostAs<UploadTempMediaResult>(_token.GetUrlWithToken(uploadTempMediaUrl), content);
        }

        private readonly string getTempMediaUrl = "cgi-bin/media/get";
        public void GetTempMedia()
        {

        }

        private readonly string sendUrl = "cgi-bin/message/custom/send";
        public void Send()
        {

        }
    }
}

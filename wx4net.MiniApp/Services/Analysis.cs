﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    Analysis
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 6:04 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 6:04 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Collections.Generic;
using System.Net.Http;
using wx4net.Extentions;
using wx4net.MiniApp.DTO.Analysis;

namespace wx4net.MiniApp.Services
{
    public class Analysis : ServiceBase
    {

        public Analysis(MiniAppConfig config, MiniAppToken token, HttpClient client)
            : base(config, token, client) { }

        #region AccessRetain
        private string getMonthlyRetainUrl = "datacube/getweanalysisappidmonthlyretaininfo";
        public RetainInfoResult GetMonthlyRetain(AnalysisRequest request)
        {
            return PostWithToken<RetainInfoResult>(getMonthlyRetainUrl, request);
        }

        private string getWeeklyRetainUrl = "datacube/getweanalysisappidweeklyretaininfo";
        public RetainInfoResult GetWeeklyRetain(AnalysisRequest request)
        {
            return PostWithToken<RetainInfoResult>(getWeeklyRetainUrl, request);
        }

        private string getDailyRetainUrl = "datacube/getweanalysisappiddailyretaininfo";
        public RetainInfoResult GetDailyRetain(AnalysisRequest request)
        {
            return PostWithToken<RetainInfoResult>(getDailyRetainUrl, request);
        }
        #endregion

        #region AccessTrend
        private string getMonthlyVisitTrendUrl = "datacube/getweanalysisappidmonthlyvisittrend";
        public List<VisitTrendInfoResult> GetMonthlyVisitTrend(AnalysisRequest request)
        {
            return PostWithToken<List<VisitTrendInfoResult>>(getMonthlyVisitTrendUrl, request);
        }

        private string getWeeklyVisitTrendUrl = "datacube/getweanalysisappidweeklyvisittrend";
        public List<VisitTrendInfoResult> GetWeeklyVisitTrend(AnalysisRequest request)
        {
            return PostWithToken<List<VisitTrendInfoResult>>(getWeeklyVisitTrendUrl, request);
        }

        private string getDailyVisitTrendUrl = "datacube/getweanalysisappiddailyvisittrend";
        public List<VisitTrendInfoResult> GetDailyVisitTrend(AnalysisRequest request)
        {
            return PostWithToken<List<VisitTrendInfoResult>>(getDailyVisitTrendUrl, request);
        }
        #endregion

        private string getUserPortraitUrl = "datacube/getweanalysisappiduserportrait";
        public UserPortraitInfoResult GetUserPortrait(AnalysisRequest request)
        {
            return PostWithToken<UserPortraitInfoResult>(getUserPortraitUrl, request);
        }

        private string getVisitDistributionUrl = "datacube/getweanalysisappidvisitdistribution";
        public VisitDistributionInfoResult GetVisitDistribution(AnalysisRequest request)
        {
            return PostWithToken<VisitDistributionInfoResult>(getVisitDistributionUrl, request);
        }

        private string getVisitPageUrl = "datacube/getweanalysisappidvisitpage";
        public VisitPageInfoResult GetVisitPage(AnalysisRequest request)
        {
            return PostWithToken<VisitPageInfoResult>(getVisitPageUrl, request);
        }

        private string getDailySummaryUrl = "datacube/getweanalysisappiddailysummarytrend";
        public DailySummaryInfoResult GetDailySummary(AnalysisRequest request)
        {
            return PostWithToken<DailySummaryInfoResult>(getDailySummaryUrl, request);
        }
    }
}

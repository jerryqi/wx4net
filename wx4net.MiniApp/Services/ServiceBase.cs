﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    ServiceBase
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 2:31 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 2:31 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Net.Http;
using wx4net.Extentions;

namespace wx4net.MiniApp.Services
{
    public abstract class ServiceBase
    {
        protected readonly MiniAppConfig _config;
        protected readonly MiniAppToken _token;
        protected readonly HttpClient _client;

        public ServiceBase(MiniAppConfig config, MiniAppToken token, HttpClient client)
        {
            _config = config;
            _token = token;
            _client = client;
        }

        protected T PostWithToken<T>(string url, object request)
        {
            return _client.PostAs<T>(_token.GetUrlWithToken(url), request.ToHttpContent());
        }

        protected T GetWithAppInfo<T>(string url, string arg = null)
        {
            return _client.GetAs<T>(string.IsNullOrEmpty(arg) ? url : _config.GetUrlWithAppInfo(url, arg));
        }
    }
}

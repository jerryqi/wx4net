﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    MiniAppConfig
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 11:48 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 11:48 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Autofac;
using wx4net.MiniApp.Services;

namespace wx4net.MiniApp
{
    public class MiniAppConfig : Config
    {
        public static void Initiallize(MiniAppConfig appConfig, ContainerBuilder builder)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            builder.RegisterInstance(httpClient).SingleInstance();
            builder.RegisterInstance(appConfig).SingleInstance();
        }

        private Dictionary<string, string> urlDics = new Dictionary<string, string>()
        {
            { "code2SessionUrl","https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=authorization_code" },
            { "getPaidUnionIdUrl","https://api.weixin.qq.com/wxa/getpaidunionid?access_token={0}&openid={1}" },
            { "getAccessTokenUrl","https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}" },
            { "getMonthlyRetainUrl","https://api.weixin.qq.com/datacube/getweanalysisappidmonthlyretaininfo?access_token=ACCESS_TOKEN" }
        };

        private string Format(string key, params object[] args)
        {
            return string.Format(urlDics[key], args);
        }

        public MiniAppConfig(string appid, string secret, Dictionary<string, string> urls = null) : base(appid, secret)
        {
            if (urls != null)
            {
                foreach (var url in urls)
                {
                    urlDics[url.Key] = url.Value;
                }
                //urlDics = urls.Concat(urlDics).ToLookup(a => a.Key, a => a.Value)
                //    .ToDictionary(g => g.Key, g => g.First());
            }
        }

        public string code2SessionUrl(string code) => Format("code2SessionUrl", AppId, Secret, code);

        public string getPaidUnionIdUrl(string token, string openid) => Format("getPaidUnionIdUrl", token, openid);

        public string getAccessTokenUrl => Format("getAccessTokenUrl", AppId, Secret);
    }
}

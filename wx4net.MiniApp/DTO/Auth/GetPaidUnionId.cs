﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    GetPaidUnionIdResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 4:14 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 4:14 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.Auth
{
    public class GetPaidUnionIdResult : ResultBase
    {
        [JsonProperty("unionid")]
        public string UnionId { get; set; }
    }

    public class GetPaidUnionIdRequest
    {
        public GetPaidUnionIdRequest(string openid, string transaction_id)
        {
            OpenId = openid;
            TransactionId = transaction_id;
        }

        public GetPaidUnionIdRequest(string openid, string mch_id, string out_trade_no)
        {
            OpenId = openid;
            MchId = mch_id;
            OutTradeNo = out_trade_no;
        }

        public string OpenId { get; private set; }

        public string TransactionId { get; private set; }

        public string MchId { get; private set; }

        public string OutTradeNo { get; private set; }

        internal string UrlPattern => string.IsNullOrEmpty(TransactionId) ?
            string.Format("openid={0}&mch_id={1}&out_trade_no={2}", OpenId, MchId, OutTradeNo) :
            string.Format("openid={0}&transaction_id={1}", OpenId, TransactionId);
    }
}

﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    GetAccessTokenResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 4:14 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 4:14 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.Auth
{
    public class GetAccessTokenResult : ResultBase
    {
        public GetAccessTokenResult()
        {
            MakeTime = DateTime.Now;
        }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonIgnore]
        public DateTime MakeTime { get; private set; }

        public bool IsExpired
            => ExpiresIn - (DateTime.Now - MakeTime).TotalSeconds > 20;
    }
}

﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    Code2SessionResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/23/2019 5:50 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/23/2019 5:50 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.Auth
{
    public class Code2SessionResult : ResultBase
    {
        [JsonProperty("session_key")]
        public string SessionKey { get; set; }

        [JsonProperty("openid")]
        public string OpenId { get; set; }

        [JsonProperty("unionid")]
        public string UnionId { get; set; }
    }
}

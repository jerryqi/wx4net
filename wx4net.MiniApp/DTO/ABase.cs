﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    ResultBase
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 11:37 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 11:37 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO
{
    public class ResultBase
    {
        [JsonProperty("errcode")]
        public int ErrCode { get; set; }

        [JsonProperty("errmsg")]
        public string ErrMsg { get; set; }
    }

    public class RequestBase { }
}

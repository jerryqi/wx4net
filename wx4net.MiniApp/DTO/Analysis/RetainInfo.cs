﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    RetainInfoResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/24/2019 6:14 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/24/2019 6:14 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.Analysis
{
    /// <summary>
    /// Access retention result model
    /// </summary>
    public class RetainInfoResult
    {
        /// <summary>
        /// Monthly retain, format:yyyyMM
        /// Weekly retain, format:yyyyMMdd-yyyyMMdd
        /// Daily retain, format:yyyyMMdd
        /// </summary>
        [JsonProperty("ref_date")]
        public string RefDate { get; set; }

        /// <summary>
        /// Added user retention
        /// </summary>
        [JsonProperty("visit_uv_new")]
        public Dictionary<int, int> VisitUVNew { get; set; }

        /// <summary>
        /// Active user retention
        /// </summary>
        [JsonProperty("visit_uv")]
        public Dictionary<int, int> VisitUV { get; set; }
    }
}

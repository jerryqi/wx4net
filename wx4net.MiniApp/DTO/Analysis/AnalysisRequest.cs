﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    AnalysisRequest
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 10:55 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 10:55 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.Analysis
{
    /// <summary>
    /// Access retention request model
    /// </summary>
    public class AnalysisRequest
    {
        public AnalysisRequest(DateTime begin, DateTime end)
        {
            BeginDate = begin.ToString("yyyyMMdd");
            EndDate = end.ToString("yyyyMMdd");
        }

        /// <summary>
        /// Query begin date, format:yyyyMMdd
        /// </summary>
        [JsonProperty("begin_date")]
        public string BeginDate { get; private set; }

        /// <summary>
        /// Query end date, format:yyyyMMdd
        /// </summary>
        [JsonProperty("end_date")]
        public string EndDate { get; private set; }
    }
}

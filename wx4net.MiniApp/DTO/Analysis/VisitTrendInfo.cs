﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    VisitTrendInfoResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 11:12 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 11:12 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.Analysis
{
    public class VisitTrendInfoResult
    {
        [JsonProperty("ref_date")]
        public string RefDate { get; set; }

        [JsonProperty("session_cnt")]
        public int SessionCnt { get; set; }

        [JsonProperty("visit_pv")]
        public int VisitPv { get; set; }

        [JsonProperty("visit_uv")]
        public int VisitUv { get; set; }

        [JsonProperty("visit_uv_new")]
        public int VisitUvNew { get; set; }

        [JsonProperty("stay_time_uv")]
        public float StayTimeUv { get; set; }

        [JsonProperty("stay_time_session")]
        public float StayTimeSession { get; set; }

        [JsonProperty("visit_depth")]
        public float VisitDepth { get; set; }
    }
}

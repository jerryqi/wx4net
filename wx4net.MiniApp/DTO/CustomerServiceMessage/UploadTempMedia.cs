﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:     UploadTempMediaResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 2:51 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 2:51 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;

namespace wx4net.MiniApp.DTO.CustomerServiceMessage
{
    public class UploadTempMediaResult:ResultBase
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("media_id")]
        public string MediaId { get; set; }

        [JsonProperty("created_at")]
        public int CreatedAt { get; set; }
    }

    public class UploadTempMediaRequest
    {

    }
}

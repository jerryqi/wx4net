﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:     SetTypingResult
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/26/2019 11:57 AM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/26/2019 11:57 AM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace wx4net.MiniApp.DTO.CustomerServiceMessage
{
    public class SetTypingResult : ResultBase { }

    public class SetTypingRequest
    {
        [JsonProperty("touser")]
        public string ToUser { get; set; }

        [JsonProperty("command")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SetType Command { get; set; }
    }

    public enum SetType
    {
        Typing,
        CancelTyping
    }
}

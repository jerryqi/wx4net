﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    MiniAppToken
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/25/2019 4:49 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/25/2019 4:49 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Net.Http;
using wx4net.Extentions;
using wx4net.MiniApp.DTO.Auth;
using wx4net.MiniApp.Services;

namespace wx4net.MiniApp
{
    public class MiniAppToken
    {
        private readonly MiniAppConfig _config;
        private readonly HttpClient _client;

        public MiniAppToken(MiniAppConfig config, HttpClient client)
        {
            _config = config;
            _client = client;
        }

        private GetAccessTokenResult tokenResult;

        public string Current
        {
            get
            {
                if (tokenResult == null || tokenResult.IsExpired)
                    GetAccessToken();
                return tokenResult.AccessToken;
            }
        }

        internal string GetUrlWithToken(string url, string arg = null)
        {
            var mergeUrl = string.Format("{0}/{1}access_token={2}", _config.ApiBase, _config.GetPattern(url), Current);
            return string.IsNullOrEmpty(arg) ? mergeUrl : string.Format("{0}&{1}", mergeUrl, arg);
        }

        private string getAccessTokenUrl = "cgi-bin/token?grant_type=client_credential";
        public GetAccessTokenResult GetAccessToken()
        {
            var url = _config.GetUrlWithAppInfo(getAccessTokenUrl);
            tokenResult = _client.GetAs<GetAccessTokenResult>(url);
            return tokenResult;
        }
    }
}

﻿// ============================================================
// Copyright (C) 2019 WSE All Rights Reserved.
// Organization: Wall Steet English
// FileName:    MiniAppUrls
// Version:      $V1.0.0.0
// Create By:    jerryqi
// Email:        jerry.qi@wallstreetenglish.com
// Create Time:  9/25/2019 4:03 PM
//
// Description:
//
// ============================================================
// Modify Mark
// Modify Time:  9/25/2019 4:03 PM
// Modify By:    jerryqi
// Version:      $V1.0.0.0
// Description:
//
// ============================================================
using System;
using System.Collections.Generic;

namespace wx4net.MiniApp
{
    public class MiniAppUrls : Urls
    {
        private readonly MiniAppConfig config;
        private readonly MiniAppToken appToken;

        public MiniAppUrls(MiniAppConfig _config, MiniAppToken _appToken)
        {
            config = _config;
            appToken = _appToken;
        }

        private Dictionary<string, string> urlDics = new Dictionary<string, string>()
        {
            { "getPaidUnionIdUrl","https://api.weixin.qq.com/wxa/getpaidunionid?access_token={0}&openid={1}" },
            { "getMonthlyRetainUrl","https://api.weixin.qq.com/datacube/getweanalysisappidmonthlyretaininfo?access_token=ACCESS_TOKEN" }
        };
    }
}
